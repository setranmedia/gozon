
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('categories', require('./components/Categories.vue'));
Vue.component('posts', require('./components/Posts.vue'));
Vue.component('products', require('./components/Products.vue'));
Vue.component('pages', require('./components/Pages.vue'));
Vue.component('gozon-burger', require('./components/GozonBurger.vue'));
Vue.component('general-settings', require('./components/settings/General.vue'));
Vue.component('permalinks-settings', require('./components/settings/Permalinks.vue'));
Vue.component('native-ads-settings', require('./components/settings/NativeAds.vue'));
Vue.component('amazon-api-settings', require('./components/settings/AmazonApi.vue'));
Vue.component('urlredirection-settings', require('./components/settings/Urlredirection.vue'));

const app = new Vue({
    el: '#gozon',
    data: {csrfToken: ''},
    created(){
      this.csrfToken = window.csrfToken;
      this.refreshToken();
    },
    methods: {
      refreshToken(){
        var _this = this;
        setTimeout(function() {
          axios.post(window.Laravel.home_url+'/check-alive').then(response => {
            _this.csrfToken = window.csrfToken = response.data.csrfToken;
            _this.refreshToken();
          });
        },(5 * 60 * 1000));
      }
    }
});
