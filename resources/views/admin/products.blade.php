@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Products</h1>
	</section>

	<section class="content">
		<products></products>
	</section>

</div>
@endsection
