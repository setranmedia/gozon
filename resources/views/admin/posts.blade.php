@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Posts</h1>
	</section>

	<section class="content">
		<posts></posts>
	</section>

</div>
@endsection
