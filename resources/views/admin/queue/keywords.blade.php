@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Keywords Queue</h1>
	</section>

	<section class="content">
		<queue-keywords></queue-keywords>
	</section>

</div>
@endsection
