@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>URL Redirection Setting</h1>
	</section>

	<section class="content">
		<urlredirection-settings></urlredirection-settings>
	</section>

</div>
@endsection
