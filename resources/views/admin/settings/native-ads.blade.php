@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Native Ads Setting</h1>
	</section>

	<section class="content">
		<native-ads-settings></native-ads-settings>
	</section>

</div>
@endsection
