@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Permalinks Setting</h1>
	</section>

	<section class="content">
		<permalinks-settings></permalinks-settings>
	</section>

</div>
@endsection
