@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>Amazon API Setting</h1>
	</section>

	<section class="content">
		<amazon-api-settings></amazon-api-settings>
	</section>

</div>
@endsection
