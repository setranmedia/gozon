@extends('admin.layout')

@section('content')
<div class="content-wrapper" id="user-content">
	<section class="content-header">
		<h1>General Setting</h1>
	</section>

	<section class="content">
		<general-settings></general-settings>
	</section>

</div>
@endsection
