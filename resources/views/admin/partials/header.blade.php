<header class="main-header">
	<a href="{!! route('admin.dashboard') !!}" class="logo"><span class="logo-mini"><b>G</b>Z</span><span class="logo-lg"><b>Go</b>Zon</span></a>

	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li>
					<a href="{!! route('admin.logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

					<form id="logout-form" action="{!! route('admin.logout') !!}" method="POST" style="display: none;"><input type="hidden" name="_token" v-model="csrfToken"></form>
				</li>
			</ul>
		</div>
	</nav>
</header>

<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li><a href="{!! route('admin.dashboard') !!}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="{!! route('admin.categories') !!}"><i class="fa fa-tags"></i> <span>Categories</span></a></li>
			<li><a href="{!! route('admin.posts') !!}"><i class="fa fa-file-code-o"></i> <span>Posts</span></a></li>
			<li><a href="{!! route('admin.products') !!}"><i class="fa fa-shopping-cart"></i> <span>Products</span></a></li>
			<li><a href="{!! route('admin.pages') !!}"><i class="fa fa-file-text-o"></i> <span>Manual Pages</span></a></li>
			<li class="treeview">
				<a href="#"><i class="fa fa-wrench"></i> <span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Profile</a></li>
					<li><a href="{!! route('admin.settings.general') !!}"><i class="fa fa-circle-o"></i> General</a></li>
					<li><a href="{!! route('admin.settings.permalinks') !!}"><i class="fa fa-circle-o"></i> Permalinks</a></li>
					<li><a href="{!! route('admin.settings.amazon-api') !!}"><i class="fa fa-circle-o"></i> Amazon API</a></li>
					<li><a href="{!! route('admin.settings.native-ads') !!}"><i class="fa fa-circle-o"></i> Native Ads</a></li>
					<li><a href="{!! route('admin.settings.urlredirection') !!}"><i class="fa fa-circle-o"></i> URL Redirection</a></li>
				</ul>
			</li>
		</ul>
	</section>
</aside>
