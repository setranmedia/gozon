<footer class="main-footer">
	<div class="container">
		<div class="pull-right hidden-xs">
			<strong>Copyright &copy; {!! date('Y') !!} <a href="{!! route('home') !!}">{!! config('app.name') !!}</a>.</strong> All rights reserved.
		</div>
	</div>
</footer>
