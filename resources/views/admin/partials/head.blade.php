<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>GoZon</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{mix('css/app.css')}}" rel="stylesheet">
  <link href="{{mix('css/style.css')}}" rel="stylesheet">
	<script>
			window.Laravel = <?php echo json_encode([
				'home_url'  => route('home'),
				'admin_url'  => route('admin.dashboard'),
			]); ?>
	</script>
</head>
