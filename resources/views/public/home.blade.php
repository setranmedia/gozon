<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper" id="gozon">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                              @include('public.partials.widgets.categories')
                              @include('public.partials.content.posts',['posts' => config('posts')])
                              @if($posts = config('posts'))
                                {{ $posts->links() }}
                              @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
    </body>
</html>
