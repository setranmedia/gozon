<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                              @if($post = config('post'))
                                <h1 class="text-center">{!! config('post.title') !!}</h1>
                                {!! $post->before_ads_content !!}
                                @include('public.partials.widgets.custom-ads')
                                {!! $post->after_ads_content !!}
                                @include('public.partials.widgets.products')
                                {!! $post->bottom_content !!}
                                <div id="gozon">
                                  <gozon-burger :burger="{!! json_encode($burger) !!}" :tracking_id="'{!! $post->tracking_id !!}'"></gozon-burger>
                                </div>
                              @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
    </body>
</html>
