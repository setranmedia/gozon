<!DOCTYPE html>
<html>
    @include('public.partials.head')
    <body class="hold-transition skin-blue fixed layout-top-nav">
        <div class="wrapper" id="amanat">
            @include('public.partials.header')
            <div class="content-wrapper">
                <div class="container">
                    <section class="content" id="main-content">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                              @if($categories = config('categories'))
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <h2 class="box-title">Categories</h2>
                                    </div>
                                    <div class="box-body">
                                      <div class="box-body no-padding">
                                        <ul class="nav nav-pills nav-stacked">
                                          @foreach($categories as $category)
                                          <li><a href="{{ $category->url }}">{!! $category->name !!}</a></li>
                                          @endforeach
                                        </ul>
                                      </div>
                                    </div>
                                </div>
                              @endif
                              @if($pages = config('pages'))
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <h2 class="box-title">Pages</h2>
                                    </div>
                                    <div class="box-body">
                                      <div class="box-body no-padding">
                                        <ul class="nav nav-pills nav-stacked">
                                          @foreach($pages as $page)
                                          <li><a href="{{ $page->url }}">{!! $page->name !!}</a></li>
                                          @endforeach
                                        </ul>
                                      </div>
                                    </div>
                                </div>
                              @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            @include('public.partials.footer')
        </div>
        <script type="text/javascript" src="{{mix('js/app.js')}}"></script>
        <script type="text/javascript" src="{{mix('js/style.js')}}"></script>
        {!! config('gozon.general.footer_script') !!}
    </body>
</html>
