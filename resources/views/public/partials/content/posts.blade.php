@if(isset($posts) && $posts)
	@foreach($posts as $post)
		<div class="box box-info">
			<div class="box-header with-border">
				<h2 class="box-title">{!! $post->title !!}</h2>
			</div>
			<div class="box-body">
				<p>{!! str_limit(strip_tags($post->before_ads_content),300) !!}</p>
				@if($products = $post->products()->inRandomOrder()->limit(4)->remember(env('CACHE_QUERY',1440))->cacheTags(['products','gozon'])->get())
					<div class="text-center">
						@foreach($products as $product)
							<a href="{{ $product->product_url }}" title="{{$product->title}}">
								<img src="{!! $product->thumb_image_url !!}" alt="{{ $product->title }}" class="img-thumbnail">
							</a>
						@endforeach
					</div>
				@endif
			</div>
			<div class="box-footer">
				<div class="pull-right">
					<a href="{!! $post->url !!}" class="btn btn-info">See Details <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
		</div>
	@endforeach
@endif
