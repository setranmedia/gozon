@if(isset($product))
<div class="box box-info">
	<div class="box-header with-border">
		<h1 class="box-title" itemprop="name">{!! $product->title !!}</h1>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				@if(is_array($product->medium_images))
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						@foreach($product->medium_images as $i => $image)
						<li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" {!! $i == 0 ? 'class="active"' :'' !!}></li>
						@endforeach
					</ol>
					<div class="carousel-inner">
						@foreach($product->medium_images as $i => $image)
						<div class="item {!! $i == 0 ? 'active' :'' !!}">
							<img src="{{ $image }}" alt="{{ $product->title }}" itemprop="image">
						</div>
						@endforeach
					</div>
					<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
						<span class="fa fa-angle-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
						<span class="fa fa-angle-right"></span>
					</a>
				</div>
				@endif
			</div>
			<div class="col-md-6">
				<table class="table">
					@if($product->original_price)
					<tr>
						<td style="vertical-align:middle;"><strong>List Price</strong></td>
						<td style="vertical-align:middle;">
							<span class="text-red"><strike>US ${!!$product->original_price !!}</strike></span>
						</td>
					</tr>
					@endif
					@if($product->sale_price)
					<tr>
						<td style="vertical-align:middle;"><strong>Sale Price</strong></td>
						<td style="vertical-align:middle;">
							<span class="text-aqua" style="font-size:20px;">US ${!! $product->sale_price !!}</span>
						</td>
					</tr>
					@endif
					@if($product->discount)
					<tr itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
						<td style="vertical-align:middle;"><strong>You Save</strong></td>
						<td style="vertical-align:middle;">
							<span class="text-red">US ${!! $product->original_price - $product->sale_price !!}</span>
							<span class="label label-info">{!! $product->discount !!}% OFF</span>
							<meta itemprop="lowPrice" content="{!! $product->sale_price !!}" />
							<meta itemprop="highPrice" content="{!! $product->original_price !!}" />
							<meta itemprop="priceCurrency" content="USD" />
						</td>
					</tr>
					@endif
					@if($product->rating)
					<tr>
						<td><strong>Rating</strong></td>
						<td itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
							<span itemprop="ratingValue">{!! $product->rating !!}</span> of 5.0 Based on <span itemprop="reviewCount">{!! $product->review_count !!}</span> Reviews
						</td>
					</tr>
					@endif
					@if($product->vendor)
					<tr>
						<td><strong>Vendor</strong></td>
						<td>
							<a href="{{ route('redirect').'?title='.urlencode($product->vendor).'&url='.urlencode($product->vendor_url) }}" title="Visit Seller Page" target="_blank">
								<span itemprop="brand">{!! $product->vendor !!}</span>
							</a>
						</td>
					</tr>
					@endif
				</table>
				<div style="margin-bottom:10px;">
					<a class="btn btn-block btn-lg btn-info" href="{{ route('redirect').'?title='.urlencode($product->title).'&url='.urlencode($product->product_url) }}"><i class="fa fa-shopping-cart"></i> Buy Now</a>
				</div>
				<p><img src="{{ url('images/payments-m.png') }}" alt="" class="img-responsive"></p>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div itemprop="description">
			{!! config('description') !!}
		</div>
	</div>
</div>
@if(is_array($product->features))
	<div class="box box-info box-solid">
		<div class="box-header with-border">
			<h4 class="box-title">Features</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<ul>
				@foreach($product->features as $feature)
				<li>{!! $feature !!}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
@if(is_array($product->props))
	<div class="box box-info box-solid">
		<div class="box-header with-border">
			<h4 class="box-title">Specifications</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-striped">
				@foreach($product->props as $prop)
				<tr>
					<td>{!! $prop['name'] !!}</td>
					<td>:</td>
					<td>{!! $prop['value'] !!}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
@endif

@if($product->editorial_review)
	<div class="box box-info box-solid">
		<div class="box-header with-border">
			<h4 class="box-title">Details</h4>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button>
			</div>
		</div>
		<div class="box-body" id="productDetails">
			<div style="display:block;overflow:auto;width:100%">
			{!! $product->editorial_review !!}
		</div>
		</div>
	</div>
@endif
@endif
