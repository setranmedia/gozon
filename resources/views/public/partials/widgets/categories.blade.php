@if(is_layout('home') && $categories = config('categories'))
  <div class="box box-info">
      <div class="box-header with-border">
          <h2 class="box-title">Categories</h2>
      </div>
      <div class="box-body">
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            @foreach($categories as $category)
            <li><a href="{{ $category->url }}">{!! $category->name !!}</a></li>
            @endforeach
          </ul>
        </div>
      </div>
  </div>
@endif
@if(is_layout('category') && $category = config('category'))
  @if($category->childs && count($category->childs))
  <div class="box box-info">
      <div class="box-header with-border">
          <h2 class="box-title">{!! $category->name !!}</h2>
      </div>
      <div class="box-body">
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            @foreach($category->childs as $child)
            <li><a href="{{ $child->url }}">{!! $child->name !!}</a></li>
            @endforeach
          </ul>
        </div>
      </div>
  </div>
  @elseif($parentChilds = $category->parent->childs)
    @if(count($parentChilds))
      <div class="box box-info">
        <div class="box-header with-border">
            <h2 class="box-title">{!! $category->parent->name !!}</h2>
        </div>
        <div class="box-body">
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              @foreach($parentChilds as $child)
              <li class="{!! $child->id == $category->id ? 'active' : '' !!}"><a href="{{ $child->url }}">{!! $child->name !!}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    @endif
  @endif
@endif
