@if($products = config('products'))
	<h2>Amazon.com: {{config('post.title')}}</h2>
	@foreach($products as $product)
		<div class="box box-info">
			<div class="box-header with-border">
				<h2 class="box-title">
					<a class="gozon-out" href="{!! $product->product_url ? $product->product_url : 'https://www.amazon.com/dp/'.$product->asin.'?tag='.config('post.tracking_id') !!}" title="{{ $product->vendor }}">
						#{!! $loop->iteration !!} {!! $product->title !!}
					</a>
				</h2>
					<div class="pull-right">
						By  <strong>{!! $product->vendor !!}</strong>
					</div>
			</div>
			<div class="box-body">
				@if(is_array($product->medium_images_url))
					<div id="carousel-product-{{ $loop->iteration  }}" class="carousel slide" data-ride="carousel" style="background-color: #00c0ef;padding: 15px;">
						<ol class="carousel-indicators">
							@foreach($product->medium_images_url as $i => $image)
								<li data-target="#carousel-product-{{ $loop->parent->iteration  }}" data-slide-to="{{ $i  }}" {!! $i == 0 ? 'class="active"' :'' !!}></li>
							@endforeach
						</ol>
						<div class="row carousel-inner">
							@foreach($product->medium_images_url as $i => $image)
								<div class="col-md-offset-2 col-md-8 item {!! $i == 0 ? 'active' :'' !!}">
									<img src="{{ $image }}" alt="{{ $product->title }}" itemprop="image">
								</div>
							@endforeach
						</div>
						<a class="left carousel-control" href="#carousel-product-{{ $loop->iteration  }}" data-slide="prev">
							<span class="fa fa-angle-left"></span>
						</a>
						<a class="right carousel-control" href="#carousel-product-{{ $loop->iteration  }}" data-slide="next">
							<span class="fa fa-angle-right"></span>
						</a>
					</div>
				@endif
				<div style="margin-top:10px;padding-top:10px;border-top: 1px solid #f4f4f4;">
					{!! $product->description !!}
				</div>
			</div>
			<div class="box-footer text-center">
				<a href="{!! $product->product_url !!}" class="btn btn-info gozon-out">Buy  from Amazon</a>
			</div>
		</div>
	@endforeach
@endif
