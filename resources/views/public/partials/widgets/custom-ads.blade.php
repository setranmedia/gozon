@if($products = config('products'))
  @if($productsId = $products->take(4)->pluck('asin')->toArray())
  <div id="amazon-native-ads">
    <script type="text/javascript">
    amzn_assoc_placement = "adunit0";
    amzn_assoc_search_bar = {!! json_encode(config('gozon.native-ads.search_bar',"true")) !!};
    amzn_assoc_tracking_id = {!! json_encode(config('gozon.native-ads.tracking_id',"setranmedia-20")) !!};
    amzn_assoc_ad_mode = "manual";
    amzn_assoc_ad_type = "smart";
    amzn_assoc_marketplace = "amazon";
    amzn_assoc_region = "US";
    amzn_assoc_title = {!! json_encode(config('gozon.native-ads.title',"Amazon Picks")) !!};
    amzn_assoc_linkid = {!! json_encode(config('gozon.native-ads.linkid',"57cc760ad65c18af1847fd02ac596bce")) !!};
    amzn_assoc_asins = {!! json_encode(implode(',',$productsId)) !!};
    </script>
    <script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>
  </div>
  @endif
@endif
