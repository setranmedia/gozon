<footer class="main-footer">
	<div class="container">
		<div class="pull-right hidden-xs">
			<strong>Copyright &copy; {!! date('Y') !!} <a href="{!! route('home') !!}">{!! config('gozon.general.site_name') !!}</a>.</strong> All rights reserved.
		</div>
	</div>
</footer>
