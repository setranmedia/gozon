<header class="main-header">
	<nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<a href="{!! route('home') !!}" class="navbar-brand">{!! config('gozon.general.site_name','GoZon Landing Page') !!}</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><i class="fa fa-bars"></i></button>
			</div>
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					@if($pages = config('pages'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Resources <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							@foreach($pages as $page)
								<li><a href="{!! $page->url !!}">{!! $page->name !!}</a></li>
							@endforeach
						</ul>
					</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
</header>
