<section class="content-header">
  <div class="row">
    <div class="col-md-offset-2 col-md-8">
      <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li>
          <a href="{!! route('home') !!}">
            <i class="fa fa-home"></i>Home
          </a>
        </li>
        @if(is_layout('product') && $product = config('product',false))
          <?php $i = 1; ?>
          @if($product->categories)
            @foreach($product->categories as $category)
              <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="{!! $category->url !!}">
                  <span itemprop="name">{!! $category->name !!}</span>
                </a>
                <meta itemprop="position" content="{!! $i !!}" />
              </li>
              <?php $i++; ?>
            @endforeach
          @endif
        @endif
      </ol>
    </div>
  </div>
</section>
