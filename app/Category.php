<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Category extends Model
{
	use Sluggable;
	use Rememberable;
	protected $guarded = ['updated_at','created_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = html_entity_decode(trim($value),ENT_QUOTES);
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    public function getUrlAttribute(){
        return route('category',[$this->slug]);
    }

		public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }
}
