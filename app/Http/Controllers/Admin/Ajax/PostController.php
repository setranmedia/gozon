<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\Product;
use App\Page;
use App\User;
use App\SetranMedia\Api AS SMApi;

class PostController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function createCategory(Request $request){
		if($request->has('category')){
			 try{
			 	$data = $request->get('category');
			 	Category::create([
					'name' => trim($data['name']),
					'parent_id' => isset($data['parent_id']) && $data['parent_id'] ?trim($data['parent_id']):null,
				]);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createPost(Request $request){
		if($request->has('post')){
			 try{
			 	$data = $request->get('post');
			 	$post = Post::create([
					'title' => trim($data['title']),
					'tracking_id' => trim($data['tracking_id']),
					'before_ads_content' => isset($data['before_ads_content'])?trim($data['before_ads_content']):null,
					'after_ads_content' => isset($data['after_ads_content'])?trim($data['after_ads_content']):null,
					'bottom_content' => isset($data['bottom_content'])?trim($data['bottom_content']):null,
				]);
				if(isset($data['categories'])){
					$post->categories()->sync($data['categories']);
				}
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateCategory(Request $request){
		if($request->has('category_id') && $request->has('category')){
			try{
				$data = $request->get('category');
				$category = Category::where('id',$request->get('category_id'))->firstOrFail();
				$category->update([
					'name' => trim($data['name']),
					'parent_id' => $data['parent_id'] ? trim($data['parent_id']) : null,
				]);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updatePost(Request $request){
		if($request->has('post_id') && $request->has('post')){
			try{
				$data = $request->get('post');
				$categories = isset($data['categories']) && !empty($data['categories']) ? $data['categories'] : [];
				if(isset($data['categories']))unset($data['categories']);
				$post = Post::where('id',$request->get('post_id'))->firstOrFail();
				$post->update($data);
				$post->categories()->sync($categories);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteCategory(Request $request){
		if($request->has('category_id')){
			try{
				$category = Category::where('id',$request->get('category_id'))->firstOrFail();
        $childs = $category->childs;
        if($childs){
          foreach($childs as $child){
            $child->parent_id = null;
						$child->save();
          }
        }
				$category->delete();
				\DB::table('category_post')->where('category_id',$category->id)->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deletePost(Request $request){
		if($request->has('post_id')){
			try{
				$post = Post::where('id',$request->get('post_id'))->firstOrFail();
				$post->delete();
				\DB::table('category_post')->where('post_id',$post->id)->delete();
				\DB::table('post_product')->where('post_id',$post->id)->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function createPage(Request $request){
		if($request->has('page')){
			 try{
			 	$data = $request->get('page');
			 	Page::create($data);
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updatePage(Request $request){
		if($request->has('page_id') && $request->has('page')){
			try{
				$data = $request->get('page');
				$page = Page::where('id',$request->get('page_id'))->first();
				$page->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deletePage(Request $request){
		if($request->has('page_id')){
			try{
				Page::where('id',$request->get('page_id'))->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateProfile(Request $request){
		if($request->has('name') && $request->has('email')){
			 try{
				$user = $request->user();
				$user->name = $request->get('name');
				$user->email = $request->get('email');
				$user->save();
				return response()->json(['success' => true]);
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updatePassword(Request $request){
		if($request->has('old') && $request->has('new')){
			 try{
				$user = $request->user();
				$credentials = ['email' => $user->email,'password' => $request->get('old')];
				if(\Auth::validate($credentials)){
					$user->password = bcrypt($request->get('new'));
					$user->save();
					return response()->json(['success' => true]);
				}else{
					return response()->json(['error' => 'Wrong current password.'],500);
				}
			 }catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			 }
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}

	public function updateGoZon(Request $request){
		if($request->has('name') && $request->has('values')){
			 try{
				 if($request->get('name') == 'urlredirection'){
					 $main = $request->get('values');
				 }else{
					 $old = config('gozon.'.$request->get('name'));
					 if(!$old || !is_array($old))
					 	return response()->json(['message' => 'Unusual activity detected'],500);
						$main = array_merge($old,$request->get('values'));
				 }
				$save_data = var_export($main, 1);
				if(\File::put(config_path() . '/gozon/'.$request->get('name').'.php', "<?php\n return $save_data ;")){
					if($request->get('name') == 'sitemaps') \Cache::tags('sitemap')->flush();
					return response()->json(['success' => true]);
				}
				return response()->json(['message' => 'Failed to save.'],500);
			 }catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			 }
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function postProduct(Request $request){
		if($request->has('post_id') && $request->has('asin') && $request->has('tracking_id')){
			try{
				$SMApi = new SMApi;
        $product = Product::where('asin',$request->get('asin'))->first();
        if(!$product){
          $results = $SMApi->getProductDetail($request->get('asin'),$request->get('tracking_id'));
          $product = new Product;
					$product->asin = $results['asin'];
          $product->post_id = $request->get('post_id');
					$product->title = $results['title'];
					$product->vendor = $results['vendor'];
          $product->product_url = $results['product_url'];
          $product->images = isset($results['images']) && is_array($results['images']) ? $results['images'] : [];
					if(strlen($results['description']) > 300){
						$product->description =  $results['description'];
					}elseif(isset($results['features']) && is_array($results['features'])){
						$d = '<ul>';
						foreach ($results['features'] as $f) {
							$d .= '<li>'.$f.'</li>';
						}
						$d .= '</ul>';
						$product->description =  $d;
					}
          $product->save();
          return response()->json(['success' => 'success']);
        }elseif($product->post_id != $request->get('post_id')){
          $new = new Product;
					$new->asin = $product->asin;
          $new->post_id = $request->get('post_id');
					$new->title = $product->title;
          $new->vendor = $product->vendor;
					$new->images = $product->images;
          $new->description = $product->description;
          $new->save();
          return response()->json(['success' => 'success']);
        }
				return response()->json(['success' => 'exists']);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function updateProduct(Request $request){
		if($request->has('product_id') && $request->has('product')){
			try{
				$data = $request->get('product');
				$product = Product::find($request->get('product_id'));
				$product->update($data);
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['message' => $e->getMessage()],500);
			}
		}
		return response()->json(['message' => 'Incomplete data submited'],500);
	}

	public function deleteProduct(Request $request){
		if($request->has('product_id')){
			try{
				$product = Product::find($request->get('product_id'));
				$product->delete();
				return response()->json(['success' => true]);
			}catch(\Exception $e){
				return response()->json(['error' => $e->getMessage()],500);
			}
		}
		return response()->json(['error' => 'Incomplete data submited'],500);
	}
}
