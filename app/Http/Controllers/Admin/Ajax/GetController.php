<?php

namespace App\Http\Controllers\Admin\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\Page;
use App\User;

class GetController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function profile(Request $request){
		return $request->user();
	}

	public function config(Request $request){
		if($request->has('name')){
			return response()->json(config($request->get('name')));
		}
		return response()->json(['error' => 'Invalid request.'],500);
	}

	public function categories(Request $request){
		try{
			return response()->json(Category::all());
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function pages(Request $request){
		try{
			return response()->json(Page::get());
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}

	public function posts(Request $request){
		try{
			return response()->json(Post::get());
		}catch(\Exception $e){
			return response()->json(['error' => $e->getMessage()],500);
		}
	}
}
