<?php

namespace App\Http\Controllers\Admin\Ajax;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Category;
use App\Post;
use App\Page;
use App\Product;

class DataTableController extends Controller
{
  public function domains(DataTables $dataTable){
    return $dataTable->collection(Domain::all())->rawColumns(['site_name','header_script','footer_script'])->make(true);
  }

  public function categories(Request $request,DataTables $dataTable){
    $parent_id = $request->has('parent_id') ? $request->get('parent_id') : null;

    return $dataTable->eloquent(Category::where('parent_id',$parent_id))
      ->addColumn('url',function($category){
        return $category->url;
      })->addColumn('childs',function($category){
        return $category->childs()->count();
      })->addColumn('parent',function($category){
        if($category->parent_id)
          return $category->parent->name;
        return '';
      })->addColumn('posts',function($category){
        return $category->posts()->count();
      })->rawColumns(['name','parent'])->make(true);
  }

  public function posts(Request $request,DataTables $dataTable){
    if($request->has('category_id') && (bool)$request->get('category_id')){
      $query = Category::find($request->get('category_id'))->posts();
    }else{
      $query = Post::query();
    }
    return $dataTable->eloquent($query)->addColumn('categories',function($p){
      return $p->categories()->pluck('name','id')->toArray();
    })->addColumn('products',function($p){
      return $p->products()->count();
    })->addColumn('url',function($p){ return $p->url; })->rawColumns(['before_ads_content','after_ads_content','bottom_content','title'])->make(true);
  }

  public function pages(DataTables $dataTable){
    return $dataTable->collection(Page::all())
      ->addColumn('url',function($page){
        return $page->url;
      })->rawColumns(['content'])->make(true);
  }

  public function products(Request $request,DataTables $dataTable){
    if($request->has('post_id') && (bool)$request->get('post_id')){
      $post = Post::find($request->get('post_id'));
      $query = $post->products();
      return $dataTable->eloquent($query)->addColumn('image_url',function($p){
        if(isset($p->images[0])) return 'https://images-na.ssl-images-amazon.com/images/I/'.$p->images[0].'._SS40_.jpg';
        return url('images/not-available.png');
      })->editColumn('product_url',function($p) use($post){
        return $p->product_url ? $p->product_url : 'https://www.amazon.com/dp/'.$p->asin.'?tag='.$post->tracking_id;
      })->rawColumns(['description','vendor','title'])->make(true);
    }
    $query = Product::query();
    return $dataTable->eloquent($query)->addColumn('image_url',function($p){
      if(isset($p->images[0])) return 'https://images-na.ssl-images-amazon.com/images/I/'.$p->images[0].'._SS40_.jpg';
      return url('images/not-available.png');
    })->editColumn('product_url',function($p){
      return $p->product_url ? $p->product_url : 'https://www.amazon.com/dp/'.$p->asin.'?tag='.$p->post->tracking_id;
    })->rawColumns(['description','vendor','title'])->make(true);
  }

  public function templates(Request $request,DataTables $dataTable){
    if($request->has('type'))
      return $dataTable->collection(Template::where('type',$request->get('type'))->get())
        ->rawColumns(['content'])
        ->make(true);

    return $dataTable->collection(Template::all())
      ->rawColumns(['content'])
      ->make(true);
  }

  public function keywords(Request $request,DataTables $dataTable){
    if($request->has('type'))
      return $dataTable->collection(Keyword::where('type',$request->get('type'))->get())
        ->rawColumns(['query'])
        ->make(true);

    return $dataTable->collection(Keyword::all())
      ->rawColumns(['query'])
      ->make(true);
  }

  public function queries(Request $request,DataTables $dataTable){
    if($request->has('type'))
      return $dataTable->collection(Query::where('type',$request->get('type'))->get())
        ->rawColumns(['query'])
        ->make(true);

    return $dataTable->collection(Query::all())
      ->rawColumns(['query'])
      ->make(true);
  }

  public function queues(Request $request,DataTables $dataTable){
    if($request->has('type')){
      if($request->get('type') == 'keywords'){
        if($request->has('status'))
          return $dataTable->collection(Queue::where('type','keywords')->where('status',$request->get('status'))->get())->make(true);
        return $dataTable->collection(Queue::where('type','keywords')->get())->make(true);
      }
      elseif($request->get('type') == 'products'){
        if($request->has('status'))
          return $dataTable->collection(Queue::where('type','products')->where('status',$request->get('status'))->get())
            ->addColumn('category',function($queue){
              $categories = Category::whereIn('id',$queue->categories)->pluck('name')->toArray();
              return implode(', ',$categories);
            })->make(true);
        return $dataTable->collection(Queue::where('type','products')->get())
          ->addColumn('category',function($queue){
            $categories = Category::whereIn('id',$queue->categories)->pluck('name')->toArray();
            return implode(', ',$categories);
          })->make(true);
      }
    }

    return $dataTable->collection(Queue::all())
      ->rawColumns(['query'])
      ->make(true);
  }

  public function redirects(DataTables $dataTable){
    $redirects = config('amanat.redirects',[]);
    $domains = Domain::pluck('domain')->toArray();
    $col = new Collection;
    foreach($domains as $domain){
      if(isset($redirects[$domain]) && is_array($redirects[$domain]) && isset($redirects[$domain]['type'])){
        if($redirects[$domain]['type'] == 'page'){
          $col->push([
            'id' => $domain,
            'type' => $redirects[$domain]['type'],
            'page' => $redirects[$domain]['page']
          ]);
        }else{
          $col->push([
            'id' => $domain,
            'type' => $redirects[$domain]['type'],
            'domain' => $redirects[$domain]['domain']
          ]);
        }
      }elseif(isset($redirects[$domain]) && $redirects[$domain] == 'www'){
        $col->push([
          'id' => $domain,
          'type' => 'www'
        ]);
      }elseif(isset($redirects[$domain]) && $redirects[$domain] == 'non_www'){
        $col->push([
          'id' => $domain,
          'type' => 'non_www'
        ]);
      }else{
        $col->push([
          'id' => $domain,
          'type' => 'none'
        ]);
      }
    }
    return $dataTable->collection($col)->make(true);
  }

  public function lockcategories(DataTables $dataTable){
    $lock = config('amanat.lockcategories',[]);
    return $dataTable->collection(Category::all())
      ->addColumn('domain',function($category) use ($lock){
        if(isset($lock[($category->slug)])) return $lock[($category->slug)];
        return '';
      })->rawColumns(['name'])->make(true);
  }
}
