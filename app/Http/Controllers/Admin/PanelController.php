<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function dashboard(Request $request){
		return view('admin.dashboard');
	}

	public function posts(Request $request){
		return view('admin.posts');
	}

	public function categories(Request $request){
		return view('admin.categories');
	}

	public function products(Request $request){
		return view('admin.products');
	}

	public function pages(Request $request){
		return view('admin.pages');
	}

	public function settingProfile(Request $request){
		return view('admin.settings.profile');
	}

	public function settingGeneral(Request $request){
		return view('admin.settings.general');
	}

	public function settingPermalinks(Request $request){
		return view('admin.settings.permalinks');
	}

	public function settingNativeAds(Request $request){
		return view('admin.settings.native-ads');
	}

	public function settingAmazonApi(Request $request){
		return view('admin.settings.amazon-api');
	}

	public function settingSitemaps(Request $request){
		return view('admin.settings.sitemaps');
	}

	public function settingUrlredirection(Request $request){
		return view('admin.settings.urlredirection');
	}
}
