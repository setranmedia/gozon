<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Theme;
use Carbon\Carbon;
use File;
Use App\Category;
Use App\Page;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class NotFoundController extends Controller
{
  protected $cache_time = 1440; //minutes
  protected $layout = '404';
  protected $pages = false;
  protected $categories = false;
  protected $domain;

  public function __construct(Request $request)
  {
    $cache_time = $this->cache_time = env('CACHE_QUERY',0);
    $this->categories = Category::whereNull('parent_id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories','gozon'])->get();
    $this->pages = Page::whereNotNull('id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages','gozon'])->get();
  }

  public function render()
  {
    Config::set('layout',$this->layout);
    Config::set('categories',$this->categories);
    Config::set('pages',$this->pages);
    \SEO::setTitle('Page Not Found');
    SEOMeta::addMeta('robots', 'noindex,nofollow');
    return response()->view('public.'.$this->layout,[],404);
  }
}
