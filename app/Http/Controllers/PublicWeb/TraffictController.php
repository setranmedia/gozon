<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Post;

class TraffictController extends Controller
{
	public function render(Request $request)
	{
		if(
			$request->has('callback_url') &&
			$request->has('source_url') &&
			$request->has('_token') &&
			$request->has('tracking_id') &&
			$request->has('post_id')
		){
			$clicked = $request->cookie('gozon-token','undefined');
			if($clicked != 'undefined' && ($clicked == ($request->get('tracking_id'))) ){
				$args = [
					'url' => $request->get('source_url'),
					'_token' => $request->get('_token'),
				];
				return redirect($request->get('callback_url').'?'.http_build_query($args));
			}
			try{
				$post = Post::whereIn('id',$request->get('post_id'))->inRandomOrder()->firstOrFail();
				return redirect($post->url)->with('gozon-id',$request->get('tracking_id'));
			}catch(\Exception $e){
				$args = [
					'url' => $request->get('source_url'),
					'_token' => $request->get('_token'),
				];
				return redirect($request->get('callback_url').'?'.http_build_query($args));
			}
		}
		abort(404);
	}
}
