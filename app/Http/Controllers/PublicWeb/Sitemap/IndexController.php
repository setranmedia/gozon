<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
Use App\Page;
use App\Post;

class IndexController extends Controller
{
    protected $query_cache;
    protected $sitemap_cache;


    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
      $cache_time = $this->query_cache = env('CACHE_QUERY',0);
    	$this->sitemap_cache = env('SITEMAP_CACHE',10080);
    }

    public function render(){
    	$key = 'index';
    	if(!$xml = \Cache::tags(['sitemap','gozon'])->get($key)){
        $xml = $this->buildSitemap();
        if(!empty($xml)) \Cache::tags(['sitemap','gozon'])->put($key,$xml,$this->sitemap_cache);
      }
    	return response()->make($xml)->header('content-type','text/xml');
    }

    protected function buildSitemap(){
      $pages = $categories = $childs = false;
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
      $cache_time = $this->query_cache;
      $categories = Category::whereNotNull('id')->orderBy('parent_id','ASC')->select('slug')->remember($this->query_cache)->cacheTags(['categories','gozon'])->get();
      $pages = Page::whereNotNull('id')->orderBy('name','ASC')->select('slug')->remember($this->query_cache)->cacheTags(['pages','gozon'])->get();
      $posts = Post::whereNotNull('id')->orderBy('created_at','ASC')->select('slug')->remember($this->query_cache)->cacheTags(['posts','gozon'])->get();

      $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
      $xml .= '<?xml-stylesheet type="text/xsl" href="'.$xls.'"?>'."\n";
      $xml .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
      $xml .= "\t".'<url>'."\n";
      $xml .= "\t\t".'<loc>'.route('home').'</loc>'."\n";
      $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
      $xml .= "\t\t".'<changefreq>daily</changefreq>'."\n";
      $xml .= "\t\t".'<priority>1</priority>'."\n";
      $xml .= "\t".'</url>'."\n";
      if($pages){
        foreach($pages as $page){
          $randomFloat = rand(60,80)/100;
          $xml .= "\t".'<url>'."\n";
          $xml .= "\t\t".'<loc>'.$page->url.'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t\t".'<changefreq>weekly</changefreq>'."\n";
          $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
          $xml .= "\t".'</url>'."\n";
        }
      }
      if($categories){
        foreach($categories as $category){
          $randomFloat = rand(60,80)/100;
          $xml .= "\t".'<url>'."\n";
          $xml .= "\t\t".'<loc>'.$category->url.'</loc>'."\n";
          $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
          $xml .= "\t\t".'<changefreq>daily</changefreq>'."\n";
          $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
          $xml .= "\t".'</url>'."\n";
        }
      }

      foreach($posts as $post){
        $randomFloat = rand(60,80)/100;
        $xml .= "\t".'<url>'."\n";
        $xml .= "\t\t".'<loc>'.$post->url.'</loc>'."\n";
        $xml .= "\t\t".'<lastmod>'.$now.'</lastmod>'."\n";
        $xml .= "\t\t".'<changefreq>weekly</changefreq>'."\n";
        $xml .= "\t\t".'<priority>'.$randomFloat.'</priority>'."\n";
        $xml .= "\t".'</url>'."\n";
      }
      $xml .= '</urlset>';
      return $xml;
    }
}
