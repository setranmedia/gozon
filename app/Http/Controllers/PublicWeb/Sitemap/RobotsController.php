<?php

namespace App\Http\Controllers\PublicWeb\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\Post;
Use App\Page;

class RobotsController extends Controller
{
    protected $sitemap_cache;

    public function __construct(Request $request)
    {
    	$this->middleware('publicWeb');
      $cache_time = $this->query_cache = env('CACHE_QUERY',0);
    	$this->sitemap_cache = env('SITEMAP_CACHE',10080);
    }

    public function render(){
    	$key = $this->domain.'.robots.txt';
    	if(!$txt = cache($key)){
        $txt = $this->buildRobots();
        if(!empty($txt)) cache([$key => $txt], $this->sitemap_cache);
      }
    	return response()->make($txt)->header('content-type','text/plain');
    }

    protected function buildRobots(){
    	$xls = url('sitemap.xsl');
    	$now = date('c', time());
    	$txt = 'User-agent: *'."\n";
      $txt .= 'Disallow: /admin/'."\n";
      $txt .= 'Disallow: /check-alive'."\n";
      $txt .= 'Disallow: /acceptTraffict'."\n";
      $txt .= 'Disallow: /receiveTraffict'."\n";
      $txt .= "\n";
      $txt .= 'Sitemap: '.route('sitemap.index')."\n";
      return $txt;
    }
}
