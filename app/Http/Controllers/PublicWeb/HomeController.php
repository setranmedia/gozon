<?php

namespace App\Http\Controllers\PublicWeb;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Category;
Use App\Product;
Use App\Post;
Use App\Page;
use App\User;

Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class HomeController extends Controller
{
  protected $cache_time; //minutes
  protected $layout = 'home';
  protected $pages = false;
	protected $categories = false;

  public function __construct(Request $request)
  {
    $cache_time = $this->cache_time = env('CACHE_QUERY',0);
    $this->categories = Category::whereNull('parent_id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories','gozon'])->get();
    $this->pages = Page::whereNotNull('id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages','gozon'])->get();
  }

  public function render(Request $request)
  {
    $page = $request->has('page') ? $request->get('page') : 1;
		$this->posts = Post::paginate(10);
    Config::set('layout',$this->layout);
    Config::set('categories',$this->categories);
    Config::set('pages',$this->pages);
    Config::set('posts',$this->posts);
    \SEO::setTitle(config('gozon.general.site_name','GoZon Landing Page'));
    \SEO::setDescription(config('gozon.general.description','GoZon Landing Page'));
    SEOMeta::addKeyword(config('gozon.general.keyword','GoZon Landing Page'));
    SEOMeta::addMeta('robots', 'index,follow');
    SEOMeta::setCanonical(route('home'));
    if($next_page_url = $this->posts->nextPageUrl()) SEOMeta::setNext($next_page_url);
    if($prev_page_url = $this->posts->previousPageUrl()) SEOMeta::setPrev($prev_page_url);
    OpenGraph::setUrl(route('home'))->addProperty('site_name', config('gozon.general.site_name','GoZon Landing Page'))->addProperty('type', 'website');
    Twitter::setType('summary');
    return response()->view('public.'.$this->layout);
  }
}
