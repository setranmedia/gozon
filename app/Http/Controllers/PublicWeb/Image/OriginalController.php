<?php

namespace App\Http\Controllers\PublicWeb\Image;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use Image;


class OriginalController extends Controller
{
  public function render(Request $request,$slug,$image_code){

    $cachedImage = Image::cache(function($image) use ($image_code){
      $client = new Client();
      $image_url = 'https://images-na.ssl-images-amazon.com/images/I/'.$image_code.'.jpg';
  		$res = $client->request('GET', $image_url, ['http_errors' => false]);
      if($res->hasHeader('Content-Type')){
        $type = $res->getHeader('Content-Type');
        if($type == 'image/jpeg' || (is_array($type) && in_array('image/jpeg',$type))){
          $content = (string) $res->getBody();
          print_r($type);
          return $image->make($content);
        }
      }
      return false;
    },(config('imagecache.lifetime')*60),false);

  }
}
