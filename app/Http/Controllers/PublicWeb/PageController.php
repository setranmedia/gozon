<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Category;
Use App\Product;
Use App\Post;
Use App\Page;
use App\User;
Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class PageController extends Controller
{
    protected $cache_time; //minutes
    protected $layout = 'page';
    protected $pages = false;
    protected $categories = false;


    public function __construct(Request $request)
    {
      $cache_time = $this->cache_time = env('CACHE_QUERY',0);
      $this->categories = Category::whereNull('parent_id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories','gozon'])->get();
      $this->pages = Page::whereNotNull('id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages','gozon'])->get();
    }

    public function render($slug)
    {
        try{
            $cache_time = $this->cache_time;
            $this->page = Page::where('slug',$slug)->remember($this->cache_time)->cacheTags(['pages','gozon'])->firstOrFail();
        }catch(\Exception $e){
            return abort(404);
        }
        Config::set('layout',$this->layout);
        Config::set('categories',$this->categories);
        Config::set('page',$this->page);
        Config::set('pages',$this->pages);

        \SEO::setTitle($this->page->name.' | '.config('gozon.general.site_name','GoZon Landing Page'));
    		\SEO::setDescription($this->page->name.', '.str_limit(strip_tags($this->page->content),100));
    		SEOMeta::addKeyword($this->page->name.', '.config('gozon.general.keyword','GoZon Landing Page'));
    		SEOMeta::addMeta('robots', 'index,follow');
    		SEOMeta::setCanonical($this->page->url);
        OpenGraph::setUrl($this->page->url)
            ->addProperty('site_name', config('gozon.general.site_name','GoZon Landing Page'))
            ->addProperty('type', 'article');
        return response()->view('public.'.$this->layout);
    }
}
