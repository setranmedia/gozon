<?php

namespace App\Http\Controllers\PublicWeb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Theme;
use Carbon\Carbon;
use File;
Use App\Category;
Use App\Product;
Use App\Post;
Use App\Page;
use App\User;
Use App\SetranMedia\Template AS SMTemplate;
Use App\SetranMedia\Api AS SMApi;
Use Config;
use SEOMeta;
use OpenGraph;
use Twitter;

class PostController extends Controller
{
    protected $cache_time; //minutes
    protected $layout = 'post';
    protected $pages = false;
    protected $categories = false;


    public function __construct(Request $request)
    {
      $cache_time = $this->cache_time = env('CACHE_QUERY',0);
      $this->categories = Category::whereNull('parent_id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['categories','gozon'])->get();
      $this->pages = Page::whereNotNull('id')->orderBy('name','ASC')->remember($this->cache_time)->cacheTags(['pages','gozon'])->get();
    }

    public function render(Request $request,$slug)
    {
        try{
            $cache_time = $this->cache_time;
            $this->post = Post::where('slug',$slug)->remember($this->cache_time)->cacheTags(['post','gozon'])->firstOrFail();
        }catch(\Exception $e){
            return abort(404);
        }
        $products = $this->post->products()->orderBy('priority','asc')->remember($this->cache_time)->cacheTags(['post','products','gozon'])->get();
        Config::set('layout',$this->layout);
        Config::set('categories',$this->categories);
        Config::set('post',$this->post);
        Config::set('products',$products);
        Config::set('pages',$this->pages);
        Config::set('gozon.native-ads.tracking_id',$this->post->tracking_id);

        \SEO::setTitle($this->post->title.' | '.config('gozon.general.site_name','GoZon Landing Page'));
    		\SEO::setDescription($this->post->title.', '.str_limit(strip_tags($this->post->description),100));
    		SEOMeta::addKeyword($this->post->title.', '.config('gozon.general.keyword','GoZon Landing Page'));
    		SEOMeta::addMeta('robots', 'index,follow');
    		SEOMeta::setCanonical($this->post->url);
        OpenGraph::setUrl($this->post->url)
            ->addProperty('site_name', config('gozon.general.site_name','GoZon Landing Page'))
            ->addProperty('type', 'article');

        $token = $request->cookie('gozon-token',false);
        $id = session('gozon-id',false);
        $burger = $id && !$token ? true : false;
        return response()->view('public.'.$this->layout,['burger' => $burger]);
    }
}
