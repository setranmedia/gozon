<?php

namespace App\Http\Controllers\PublicWeb\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GetController extends Controller
{
    public function checkAlive(){
      $loggedIn = false;
    	if(Auth::check()){
    		$loggedIn = true;
    	}
    	return response()->json(['loggedIn' => $loggedIn,'csrfToken' => csrf_token()]);
    }

    public function acceptTraffict(Request $request){
      $tracking_id = $request->has('tracking_id') ? $request->get('tracking_id') : 'undefined';
      return response()->json([])->cookie('gozon-token',base64_encode($tracking_id),30);
    }
}
