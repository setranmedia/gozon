<?php

namespace App\Http\Middleware;

use Closure;
use App\Domain;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($domain = config('setranmedia.domain')){
            $host = $request->header('host');
            if(str_replace('www.','',$host) != $domain){
                $request->headers->set('host', $domain);
                return redirect($request->path());
            }
        }else{
            return abort(403,"Unlicensed script detected.");
        }
        return $next($request);
    }
}
