<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App\Domain;

class PublicWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        spp_setinfo();
        $host = $request->header('host');
        $domain = str_replace('www.','',$host);
        $urls = config('gozon.urlredirection',false);
        if($urls && !empty($urls)){
          $check = array_pluck($urls,'from');
          $url = url()->current();
          if(in_array($url,$check)){
            $full = url()->full();
            $a = array_first($urls, function($value,$key) use ($url){
              return $value['from'] == $url;
            });
            $to = str_replace($a['from'],$a['to'],$full);
            return redirect($to,$a['type']);
          }
        }
        return $next($request);
    }
}
