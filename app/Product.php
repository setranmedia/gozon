<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Product extends Model
{
  use Rememberable;

  protected $guarded = ['updated_at','created_at'];
  protected $casts = [
      'images' => 'array',
  ];

  public function post()
  {
      return $this->belongsTo('App\Post');
  }

  public function getImageUrlAttribute(){
    if($this->images && isset($this->images[0])){
      if(filter_var($this->images[0], FILTER_VALIDATE_URL) === FALSE)
        return 'https://images-na.ssl-images-amazon.com/images/I/'.$this->images[0].'.jpg';
      $this->images[0];
    }
    return url('images/not-available.png');
  }

  public function getImagesUrlAttribute(){
    if($this->images && is_array($this->images)){
      $images = $this->images;
      $images = array_map(function($image){
        if(filter_var($image, FILTER_VALIDATE_URL) === FALSE)
          return 'https://images-na.ssl-images-amazon.com/images/I/'.$image.'.jpg';
        return $image;
      },$images);
      return $images;
    }
    return [url('images/not-available.png')];
  }

  public function getThumbImageUrlAttribute(){
    if($this->images && isset($this->images[0])){
      if(filter_var($this->images[0], FILTER_VALIDATE_URL) === FALSE){
        $width = config('gozon.image.thumb',150);
        return 'https://images-na.ssl-images-amazon.com/images/I/'.$this->images[0].'._SS'.$width.'_.jpg';
      }
      return $this->images[0];
    }
    return url('images/not-available.png');
  }

  public function getThumbImagesUrlAttribute(){
    if($this->images && is_array($this->images)){
      $width = config('gozon.image.thumb',150);
      $images = $this->images;
      $images = array_map(function($image) use($width){
        if(filter_var($image, FILTER_VALIDATE_URL) === FALSE)
          return 'https://images-na.ssl-images-amazon.com/images/I/'.$image.'._SS'.$width.'_.jpg';
        return $image;
      },$images);
      return $images;
    }
    return [url('images/not-available.png')];
  }

  public function getMediumImageUrlAttribute(){
    if($this->images && isset($this->images[0])){
      if(filter_var($this->images[0], FILTER_VALIDATE_URL) === FALSE){
        $width = config('gozon.image.medium',500);
        return 'https://images-na.ssl-images-amazon.com/images/I/'.$this->images[0].'._SS'.$width.'_.jpg';
      }
      return $this->images[0];
    }
    return url('images/not-available.png');
  }

  public function getMediumImagesUrlAttribute(){
    if($this->images && is_array($this->images)){
      $width = config('amanat.image.medium',500);
      $images = $this->images;
      $images = array_map(function($image) use($width){
        if(filter_var($image, FILTER_VALIDATE_URL) === FALSE)
          return 'https://images-na.ssl-images-amazon.com/images/I/'.$image.'._SS'.$width.'_.jpg';
        return $image;
      },$images);
      return $images;
    }
    return [url('images/not-available.png')];
  }

  public function getLargeImageUrlAttribute(){
    if($this->images && isset($this->images[0])){
      if(filter_var($this->images[0], FILTER_VALIDATE_URL) === FALSE){
        $width = config('gozon.image.large',750);
        return 'https://images-na.ssl-images-amazon.com/images/I/'.$this->images[0].'._SS'.$width.'_.jpg';
      }
      return $this->images[0];
    }
    return url('images/not-available.png');
  }

  public function getLargeImagesUrlAttribute(){
    if($this->images && is_array($this->images)){
      $width = config('gozon.image.large',750);
      $images = $this->images;
      $images = array_map(function($image) use($width){
        if(filter_var($image, FILTER_VALIDATE_URL) === FALSE)
          return 'https://images-na.ssl-images-amazon.com/images/I/'.$image.'._SS'.$width.'_.jpg';
        return $image;
      },$images);
      return $images;
    }
    return [url('images/not-available.png')];
  }
}
