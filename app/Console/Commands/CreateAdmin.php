<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gozon:create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin Login Detail';
    protected $db_connection = false;
    protected $db_table_exist = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkDbConnection();
        $this->checkMigration();
        $this->createUser();
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->line('*<error>Gagal konek meng DB</error> : Jal cek konfigurasi DBne maning nang file <comment>\'.env\'</comment> wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('users'))
                $this->db_table_exist = true;
            else
                $this->line('*<error>Tabel \'users\' ora ana je</error>... migrasi DBne salah mbok... jal running kiye <comment>\'php artisan migrate\'</comment>.');
        }
    }

    protected function createUser(){
        if($this->db_connection && $this->db_table_exist){
            $this->comment('**************************************');
            $this->comment('*             Gawe Admin             *');
            $this->comment('**************************************');
            $name = $this->ask('Jeneng');
            $email = $this->ask('Email');
            $password = $this->getPassword();
            try{
                $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt($password),
                ]);
                $this->info('*Adminne sukses digawe');
            }catch(\Exception $e){
                $this->error('*Gagal gawe admin');
            }
        }
    }

    protected function getPassword(){
        $password = $this->secret('Password');
        $retype = $this->secret('Baleni Passworde');
        if($password == $retype){
            return $password;
        }else{
            $this->error('*password ora pada');
            return $this->getPassword();
        }
    }
}
