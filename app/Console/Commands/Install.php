<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SetranMedia\Api AS SetranMediaApi;

use App\User;
use App\Seo;
use App\Template;
use App\Category;
use App\Domain;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gozon:install {--refresh-token=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'GoZon Installation';

    protected $db_connection = false;
    protected $db_migrated = false;
    protected $status = false;
    protected $domain;
    protected $setranmedia;
    protected $user;
    protected $SetranMediaApi;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->setranmedia = config('setranmedia');
        $this->SetranMediaApi = new SetranMediaApi;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->generateKey();
      $this->checkUser();
      $this->checkDomain();
      $this->migrateDB();
      $this->createUser();
    }

    protected function generateKey(){
      if(!isset($this->setranmedia['install']['key'])){
        $this->setranmedia['install']['key'] = true;
        $this->call('key:generate');
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['key']);
            $this->generateKey();
          }
        }
      }else{
        $this->comment('*GoZon KEY: wis digenerate');
      }
    }

    protected function checkUser(){
      $refresh = $this->option('refresh-token');
      if(!isset($this->setranmedia['install']['user']) || $refresh){
        if(!isset($this->setranmedia['user_token'])){
          $this->setranmedia['user_token'] = $this->ask('SetranMedia User Token');
        }
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->setranmedia['install']['user'] = $this->SetranMediaApi->checkUser();
          \Config::set('setranmedia',$this->setranmedia);
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['user']);
            if ($this->confirm('Arep direset tokene?')) {
              unset($this->setranmedia['user_token']);
            }
            $this->checkUser();
          }
        }
      }else{
        $this->comment('*Setran Media User Authentication: wis divalidasi.');
      }
    }

    protected function checkDomain(){
      $refresh = $this->option('refresh-token');
      if(!isset($this->setranmedia['install']['domain']) || $refresh){
        $this->setranmedia['type'] = 'landing_page';
        if(!isset($this->setranmedia['domain'])){
          $this->comment('*Catatan: domain sing dadi dashboard adminne.');
          $this->setranmedia['domain'] = $this->ask('Domain');
        }
        \Config::set('setranmedia',$this->setranmedia);
        try{
          $this->setranmedia['install']['domain'] = $this->SetranMediaApi->registerDomain();
          \Config::set('setranmedia',$this->setranmedia);
          $this->SetranMediaApi->saveConfig();
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
          if ($this->confirm('Arep njajal dibaleni maning?')) {
            unset($this->setranmedia['install']['domain']);
            if ($this->confirm('Arep direset data domaine?')) {
              unset($this->setranmedia['domain']);
            }
            $this->checkDomain();
          }
        }
      }else{
        $this->comment('*Domain: wis terdaftar.');
      }
    }

    protected function migrateDB(){
      if(isset($this->setranmedia['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && !$this->db_migrated){
          try{
            $this->call('migrate',['--force'=>true]);
          }catch(\Exception $e){
            $this->error('*Error : '.$e->getMessage());
          }
        }elseif($this->db_migrated){
          $this->line('<comment>*DB wis dimigrasi. Nek esih ana sing kurang jal running kiye</comment> \'<info>php artisan migrate</info>\'');
        }
      }
    }

    protected function createUser(){
      if(isset($this->setranmedia['install']['domain'])){
        $this->checkDbConnection();
        $this->checkMigration();
        if($this->db_connection && $this->db_migrated){
          $users = User::all(['id','name'])->count();
          if(!($users)){
            $this->call('gozon:create-admin');
          }
        }
      }
    }

    protected function checkDbConnection(){
        try {
            \DB::connection()->getPdo();
            $this->db_connection = true;
        }catch (\Exception $e) {
            $this->error('*Gagal konek meng DB : Jal cek konfigurasi DBne maning nang file \'.env\' wis bener apa urung.');
            $this->db_connection = false;
        }
    }

    protected function checkMigration(){
        if($this->db_connection){
            if(\Schema::hasTable('migrations'))
                $this->db_migrated = true;
        }
    }
}
