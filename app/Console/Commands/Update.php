<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SetranMedia\Api AS SetranMediaApi;

use App\Queue;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gozon:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade utility controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->current_version = config('setranmedia.version','0.1');
        $this->update_version = config('app.version','0.1');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if($this->current_version < $this->update_version){
        $setranmedia = config('setranmedia');
        $setranmedia['user_token'] = $this->ask('SetranMedia User Token');
        try{
          $this->SetranMediaApi = new SetranMediaApi;
          \Config::set('setranmedia',$setranmedia);
          $this->SetranMediaApi->checkUser();
          $this->output->write('# Run database migration...');
          $this->call('migrate',['--force' => true]);

          $setranmedia['version'] = config('app.version','0.1');
          $save_data = var_export($setranmedia, 1);
      		\File::put(config_path() . '/setranmedia.php', "<?php\n return $save_data ;");
          $this->info('# Update complete');
        }catch (\Exception $e) {
          $this->error('Error wa: '.$e->getMessage());
        }
      }else{
        $this->info('Nothing to update');
      }
    }
}
