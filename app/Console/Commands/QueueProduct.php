<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\SetranMedia\Api AS SMApi;
use App\Queue;
use App\Product;

class QueueProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amanat:queue-products {--delay=}';
    protected $per_execution;
    protected $queue;
    protected $SMApi;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate results and inject products from queues table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->per_execution = env('QUEUE_PER_EXECUTION',10);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $delay = $this->option('delay');
      if(!$delay) $delay = $this->ask('Delay pirang detik');
        try{
            $update = false;
            $this->comment('Sit...Enteni...Njiot random list queue...');
            $this->queue = Queue::where('type','products')->where('status','queue')->inRandomOrder()->firstOrFail();
            $this->comment('Ngacak product...');
            $filename = 'queues'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$this->queue->id.'.json';
            if(!\Storage::exists($filename)){
                $this->error('File product ora ketemu... dibusek yh?');
                die();
            }
            $products = json_decode(\Storage::get($filename),true);
            if(!is_array($products)){
                $this->error('Isi file product ora kewaca... Diedit nang koe yh?');
                die();
            }
            shuffle($products);
            $chunk = array_chunk($products, $this->per_execution);
            if(isset($chunk[0])){
                $this->SMApi = new SMApi(true);
                foreach($chunk[0] as $product_id){
                  sleep($delay);
                  $product_id = strtoupper($product_id);
                  $this->output->write('Mroses Product <comment>ASIN: '.$product_id.'</comment>...');
                  if(!$product = Product::where('asin',$product_id)->first()){
                    try{
                      $results = $this->executeProduct($product_id);
                      $product = new Product;
                      $product->asin = $product_id;
            					$product->title = $results['title'];
                      $product->vendor = $results['vendor'];
                      $product->rating = $results['rating'] ? $results['rating'] : 0;
                      $product->image = isset($results['images']) && isset($results['images'][0]) ? $results['images'][0] : NULL;
                      $product->original_price = $results['original_price'];
                      $product->sale_price = $results['sale_price'];
            					$product->discount = $results['discount'] ? $results['discount'] : 0;
                      $product->review_count = $results['review_count'] ? $results['review_count'] : 0;
                      $product->details = $results;
                      $product->save();
                      $product->categories()->sync($this->queue->categories);
                      $this->queue->success += 1;
                      $this->queue->executed += 1;
                      $this->output->writeln('<info>OK</info>');
                    }catch(\Exception $e){
                      $this->queue->failed += 1;
                      $this->queue->executed += 1;
                      $this->output->writeln('<error>'.$e->getMessage().'</error>');
                    }
                  }else{
                    $ids = $product->categories()->pluck('id')->toArray();
                    $check = array_where($this->queue->categories, function($value) use ($ids) { return !in_array($value,$ids); });
                    if(!empty($check)){
                      foreach($check as $c){
                        $ids[] = $c;
                      }
                      $product->categories()->sync($ids);
                      $this->queue->success += 1;
                      $this->output->writeln('<error>Wis ana, tapi nambah kategori</error>');
                    }else{
                      $this->queue->failed += 1;
                      $this->output->writeln('<error>Wis ana</error>');
                    }
                    $this->queue->executed += 1;
                  }
                }
                $update = true;
                array_forget($chunk, 0);
                if(is_array($chunk)){
                    $newproducts = [];
                    foreach ($chunk as $value) {
                        $newproducts = array_merge($newproducts,$value);
                    }
                    if(!empty($newproducts)){
                        $newproducts = array_values($newproducts);
                        \Storage::put($filename,json_encode($newproducts));
                    }else{
                        $this->queue->status = 'complete';
                    }
                }
                $this->queue->save();
            }
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }

    }

    protected function executeProduct($product_id){
        try{
            return $this->SMApi->getProductDetail($product_id);
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
