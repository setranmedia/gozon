<?php

namespace App\SetranMedia;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Exception;
use Carbon\Carbon;
use ApaiIO\ApaiIO;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Request\GuzzleRequest;
use ApaiIO\ResponseTransformer\XmlToArray;
use AmazonProduct;

class Api
{
	protected $api_url;
	protected $type = 'landing_page';
	protected $lang = 'en';
	public $setranmedia;
	public $cache;
	public $cache_days = 30;

	public function __construct($cache = false){
		$this->api_url = env('API_URL','https://www.setranmedia.com/api/amazon');
		$this->setranmedia = config('setranmedia');
		$this->cache = $cache;
	}

	public function checkUser(){
		$user_token = config('setranmedia.user_token',false);
		if(!$user_token) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/checkUser', [
			'query' => [
				'user_token' => $user_token,
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return $r;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function registerDomain(){
		$setranmedia = config('setranmedia',false);
		if(!$setranmedia) return false;
		$client = new Client();
		$res = $client->request('GET', $this->api_url.'/addDomain', [
			'query' => $setranmedia,
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = json_decode($response,true);
			if($status == 200) return true;
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
		}
		throw new Exception('undefined_error');
	}

	public function refreshToken(){
		$client = new Client();
		$setranmedia = config('setranmedia',false);
		if(!$setranmedia || !isset($setranmedia['domain'])) throw new Exception('Undefined configuration');
		$res = $client->request('GET', $this->api_url.'/refreshToken', [
			'query' => [
				'domain' => $setranmedia['domain'],
			],
			'http_errors' => false
		]);
		$status = $res->getStatusCode();
		if($status == 200 && $this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			$setranmedia['user_token'] = $r['user_token'];
			\Config::set('setranmedia',$setranmedia);
			$this->saveConfig();
		}elseif($this->isJson($res->getBody())){
			$response = $res->getBody();
			$r = (json_decode($response,true));
			if(isset($r['error_message'])) throw new Exception($r['error_message']);
			else throw new Exception($status);
		}else{
			throw new Exception('undefined_error');
		}
	}

	public function getProductDetail($asin,$tag=false){
		$client = new Client();
		$request = new GuzzleRequest($client);
		$request->setScheme('https');
		$config = config('gozon.amazon-api',false);
		if(!$config) throw new Exception('Amazon Credential not defined');
		$conf = new GenericConfiguration();

		$conf->setCountry($config['country'])
			->setAccessKey($config['api_key'])
			->setSecretKey($config['api_secret_key'])
			->setAssociateTag($tag)
			->setResponseTransformer(new XmlToArray())
			->setRequest($request);
		$apaiio = new ApaiIO($conf);
		AmazonProduct::config($apaiio);
		try{
			$response = AmazonProduct::item($asin);
		}catch(\Exception $e){
			throw new Exception($e->getMessage());
		}
		if($r = array_get($response,'Items.Item')){
			$results = [
				'asin' 	=> array_get($r,'ASIN',null),
				'title' 	=> array_get($r,'ItemAttributes.Title',null),
				'features' 	=> array_get($r,'ItemAttributes.Feature',null),
				'vendor' 	=> array_get($r,'ItemAttributes.Publisher',null),
				'product_url' 	=> array_get($r,'DetailPageURL',null),
				'description' 	=> array_get($r,'EditorialReviews.EditorialReview.Content',null),
				'images'	=> []
			];
			$images = array_get($r,'ImageSets.ImageSet',null);
			if(is_array($images)){
				foreach($images as $image){
					if($i = array_get($image,'HiResImage.URL')){
						$j = explode('/images/I/',$i);
						if(isset($j[1])){
							$k = explode('.jpg',$j[1]);
							if(isset($k[0])) $results['images'][] = $k[0];
						}
					}
				}
			}
			return $results;
		}
		throw new Exception('undefined_error');
	}

	public function saveConfig(){
		$config = config('setranmedia',false);
		if(!$config) throw new Exception('undefined_error');
		if(!isset($config['version'])) $config['version'] = config('app.version','0.1');
		$save_data = var_export($config, 1);
		\File::put(config_path() . '/setranmedia.php', "<?php\n return $save_data ;");
	}

	protected function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
