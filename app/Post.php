<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Watson\Rememberable\Rememberable;
use Storage;

class Post extends Model
{
  use Sluggable;
  use Rememberable;

  protected $guarded = ['updated_at','created_at'];

  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  public function getUrlAttribute(){
    return route('post',[$this->slug]);
  }

  public function categories()
  {
      return $this->belongsToMany('App\Category')->orderBy('parent_id','ASC');
  }

  public function products()
  {
      return $this->hasMany('App\Product');
  }
}
