<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',['as' => 'home', 'uses' => 'PublicWeb\HomeController@render']);
Route::get(config('gozon.permalinks.post','post/{slug}.html'),['as' => 'post', 'uses' => 'PublicWeb\PostController@render']);
Route::get(config('gozon.permalinks.resources','resources'),['as' => 'resources', 'uses' => 'PublicWeb\ResourcesController@render']);
Route::get(config('gozon.permalinks.category','category/{slug}.html'),['as' => 'category', 'uses' => 'PublicWeb\CategoryController@render']);
Route::get(config('gozon.permalinks.page','page/{slug}.html'),['as' => 'page', 'uses' => 'PublicWeb\PageController@render']);
Route::get(config('gozon.permalinks.image','images/{slug}_{image_code}.jpg'),['as' => 'image', 'uses' => 'PublicWeb\Image\OriginalController@render']);

Route::get('sitemap.xml',['as' => 'sitemap.index', 'uses' => 'PublicWeb\Sitemap\IndexController@render']);
Route::get('/robots.txt',['as' => 'robots.txt', 'uses' => 'PublicWeb\Sitemap\RobotsController@render']);

Route::post('/check-alive',['as' => 'public.ajax.get.checkAlive', 'uses' => 'PublicWeb\Ajax\GetController@checkAlive']);
Route::post('/acceptTraffict',['as' => 'public.ajax.get.acceptTraffict', 'uses' => 'PublicWeb\Ajax\GetController@acceptTraffict']);
Route::get('/receiveTraffict',['as' => 'public.ajax.get.receiveTraffict', 'uses' => 'PublicWeb\TraffictController@render']);
