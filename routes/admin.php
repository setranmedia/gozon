<?php


Route::get('/',['as' => 'admin.dashboard', 'uses' => 'Admin\PanelController@dashboard']);

Route::get('/login',['as' => 'admin.login', 'uses' => 'Admin\LoginController@showLoginForm']);
Route::post('/login',['as' => 'admin.authenticate', 'uses' => 'Admin\LoginController@login']);
Route::post('/logout',['as' => 'admin.logout', 'uses' => 'Admin\LoginController@logout']);

Route::get('/',['as' => 'admin.dashboard', 'uses' => 'Admin\PanelController@dashboard']);
Route::get('/categories',['as' => 'admin.categories', 'uses' => 'Admin\PanelController@categories']);
Route::get('/posts',['as' => 'admin.posts', 'uses' => 'Admin\PanelController@posts']);
Route::get('/products',['as' => 'admin.products', 'uses' => 'Admin\PanelController@products']);
Route::get('/pages',['as' => 'admin.pages', 'uses' => 'Admin\PanelController@pages']);
Route::get('/settings/profile',['as' => 'admin.settings.profile', 'uses' => 'Admin\PanelController@settingProfile']);
Route::get('/settings/general',['as' => 'admin.settings.general', 'uses' => 'Admin\PanelController@settingGeneral']);
Route::get('/settings/permalinks',['as' => 'admin.settings.permalinks', 'uses' => 'Admin\PanelController@settingPermalinks']);
Route::get('/settings/native-ads',['as' => 'admin.settings.native-ads', 'uses' => 'Admin\PanelController@settingNativeAds']);
Route::get('/settings/amazon-api',['as' => 'admin.settings.amazon-api', 'uses' => 'Admin\PanelController@settingAmazonApi']);
Route::get('/settings/urlredirection',['as' => 'admin.settings.urlredirection', 'uses' => 'Admin\PanelController@settingUrlredirection']);

Route::post('/ajax/get/profile',['as' => 'admin.ajax.get.profile', 'uses' => 'Admin\Ajax\GetController@profile']);
Route::post('/ajax/get/config',['as' => 'admin.ajax.get.config', 'uses' => 'Admin\Ajax\GetController@config']);
Route::post('/ajax/get/categories',['as' => 'admin.ajax.get.categories', 'uses' => 'Admin\Ajax\GetController@categories']);
Route::post('/ajax/get/posts',['as' => 'admin.ajax.get.posts', 'uses' => 'Admin\Ajax\GetController@posts']);
Route::post('/ajax/get/pages',['as' => 'admin.ajax.get.pages', 'uses' => 'Admin\Ajax\GetController@pages']);

Route::post('/ajax/post/updateProfile',['as' => 'admin.ajax.post.updateProfile', 'uses' => 'Admin\Ajax\PostController@updateProfile']);
Route::post('/ajax/post/updatePassword',['as' => 'admin.ajax.post.updatePassword', 'uses' => 'Admin\Ajax\PostController@updatePassword']);
Route::post('/ajax/post/updateGoZon',['as' => 'admin.ajax.post.updateGoZon', 'uses' => 'Admin\Ajax\PostController@updateGoZon']);

Route::post('/ajax/post/createCategory',['as' => 'admin.ajax.post.createCategory', 'uses' => 'Admin\Ajax\PostController@createCategory']);
Route::post('/ajax/post/updateCategory',['as' => 'admin.ajax.post.updateCategory', 'uses' => 'Admin\Ajax\PostController@updateCategory']);
Route::post('/ajax/post/deleteCategory',['as' => 'admin.ajax.post.deleteCategory', 'uses' => 'Admin\Ajax\PostController@deleteCategory']);

Route::post('/ajax/post/createPost',['as' => 'admin.ajax.post.createPost', 'uses' => 'Admin\Ajax\PostController@createPost']);
Route::post('/ajax/post/updatePost',['as' => 'admin.ajax.post.updatePost', 'uses' => 'Admin\Ajax\PostController@updatePost']);
Route::post('/ajax/post/deletePost',['as' => 'admin.ajax.post.deletePost', 'uses' => 'Admin\Ajax\PostController@deletePost']);

Route::post('/ajax/post/createPage',['as' => 'admin.ajax.post.createPage', 'uses' => 'Admin\Ajax\PostController@createPage']);
Route::post('/ajax/post/updatePage',['as' => 'admin.ajax.post.updatePage', 'uses' => 'Admin\Ajax\PostController@updatePage']);
Route::post('/ajax/post/deletePage',['as' => 'admin.ajax.post.deletePage', 'uses' => 'Admin\Ajax\PostController@deletePage']);

Route::post('/ajax/post/postProduct',['as' => 'admin.ajax.post.postProduct', 'uses' => 'Admin\Ajax\PostController@postProduct']);
Route::post('/ajax/post/updateProduct',['as' => 'admin.ajax.post.updateProduct', 'uses' => 'Admin\Ajax\PostController@updateProduct']);
Route::post('/ajax/post/updateRedirect',['as' => 'admin.ajax.post.updateRedirect', 'uses' => 'Admin\Ajax\PostController@updateRedirect']);
Route::post('/ajax/post/deleteProduct',['as' => 'admin.ajax.post.deleteProduct', 'uses' => 'Admin\Ajax\PostController@deleteProduct']);


Route::post('/ajax/datatable/categories',['as' => 'admin.ajax.datatable.categories', 'uses' => 'Admin\Ajax\DataTableController@categories']);
Route::post('/ajax/datatable/posts',['as' => 'admin.ajax.datatable.posts', 'uses' => 'Admin\Ajax\DataTableController@posts']);
Route::post('/ajax/datatable/products',['as' => 'admin.ajax.datatable.products', 'uses' => 'Admin\Ajax\DataTableController@products']);
Route::post('/ajax/datatable/pages',['as' => 'admin.ajax.datatable.pages', 'uses' => 'Admin\Ajax\DataTableController@pages']);
